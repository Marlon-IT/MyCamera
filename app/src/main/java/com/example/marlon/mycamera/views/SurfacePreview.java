package com.example.marlon.mycamera.views;

import android.content.Context;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

import java.io.IOException;

import static com.example.marlon.mycamera.utils.CameraUtils.releaseCamera;

/**
 * @author Marlon
 * @date 2018/1/2
 */

public class SurfacePreview extends SurfaceView implements Callback {
    private static final String TAG = SurfacePreview.class.getSimpleName();
    private SurfaceHolder mHolder;
    public Camera mCamera;
    private Camera.Parameters mParameters;

    public SurfacePreview(Context context, Camera camera) {
        super(context);
        this.mCamera = camera;
        // Install a SurfaceHolder.Callback so we get notified when the underlying surface is created and destroyed.
        //安装一个SurfaceHolder 的回调，当底层的surface被创建和销毁时，可以得到通知。
        mHolder = getHolder();
        mHolder.setFormat(PixelFormat.TRANSPARENT);
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    private void setStartPreview(Camera camera, SurfaceHolder holder) {
        try {
            camera.setPreviewDisplay(holder); // 绑定Camera和SurfaceHolder
            camera.setDisplayOrientation(90); // 旋转90度才是正常画面
            camera.startPreview();
        } catch (IOException e) {
            Log.e(TAG, "setStartPreview: setPreviewDisplay error");
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "surfaceCreated() is called");
        // Open the Camera in preview mode
        setStartPreview(mCamera, holder);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        Log.d(TAG, "surfaceChanged() is called");
        if (mCamera != null) {
            mCamera.stopPreview();
            if (mHolder != null) {
                setStartPreview(mCamera, mHolder);
            }
        }
    }


    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        releaseCamera(mCamera);
        Log.d(TAG, "surfaceDestroyed() is called");
    }

}
