package com.example.marlon.mycamera.activity;

import android.annotation.TargetApi;
import android.media.ImageReader;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.marlon.mycamera.R;
import com.wonderkiln.camerakit.CameraView;

@TargetApi(Build.VERSION_CODES.KITKAT)
public class CameraKitActivity extends AppCompatActivity implements ImageReader.OnImageAvailableListener{

    private CameraView mCameraView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_kit);
        mCameraView = findViewById(R.id.cameraView);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mCameraView.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCameraView.stop();
    }


    @Override
    public void onImageAvailable(ImageReader reader) {

    }
}
