package com.example.marlon.mycamera.activity;

import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import com.example.marlon.mycamera.R;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.marlon.mycamera.utils.CameraUtils.getPreviewDegree;

public class Camera1Activity extends AppCompatActivity implements SurfaceHolder.Callback, Camera.PreviewCallback {
    private static final String TAG = "Camera1Activity";
    @BindView(R.id.surfaceView)
    SurfaceView surfaceView;
    @BindView(R.id.takephoto)
    ImageView takephoto;
    @BindView(R.id.change)
    Switch change;
    @BindView(R.id.auto)
    RadioButton auto;
    @BindView(R.id.open)
    RadioButton open;
    @BindView(R.id.close)
    RadioButton close;
    @BindView(R.id.flash_rg)
    RadioGroup flashRg;


    private SurfaceHolder holder;
    //声明相机
    private Camera camera;
    //照片保存路径
    private String filepath = "";
    //代表摄像头的方位，CAMERA_FACING_FRONT前置      CAMERA_FACING_BACK后置
    private int cameraPosition = Camera.CameraInfo.CAMERA_FACING_BACK;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //取消标题栏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //取消状态栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //设置手机屏幕朝向，一共有7种
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        /***SCREEN_ORIENTATION_BEHIND： 继承Activity堆栈中当前Activity下面的那个Activity的方向
         SCREEN_ORIENTATION_LANDSCAPE： 横屏(风景照) ，显示时宽度大于高度
         SCREEN_ORIENTATION_PORTRAIT： 竖屏 (肖像照) ， 显示时高度大于宽度
         SCREEN_ORIENTATION_SENSOR  由重力感应器来决定屏幕的朝向,它取决于用户如何持有设备,当设备被旋转时方向会随之在横屏与竖屏之间变化
         SCREEN_ORIENTATION_NOSENSOR： 忽略物理感应器——即显示方向与物理感应器无关，不管用户如何旋转设备显示方向都不会随着改变("unspecified"设置除外)
         SCREEN_ORIENTATION_UNSPECIFIED： 未指定，此为默认值，由Android系统自己选择适当的方向，选择策略视具体设备的配置情况而定，因此不同的设备会有不同的方向选择
         SCREEN_ORIENTATION_USER： 用户当前的首选方向 **/
        setContentView(R.layout.activity_camera1);
        ButterKnife.bind(this);
        //获得句柄
        holder = surfaceView.getHolder();
        //添加回调
        holder.addCallback(this);
        //surfaceview不维护自己的缓冲区，等待屏幕渲染引擎将内容推送到用户面前
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        // 屏幕常亮
        holder.setKeepScreenOn(true);
        //设置监听
        change.setOnCheckedChangeListener(listener);
        flashRg.setOnCheckedChangeListener((group, checkedId) -> {
            if (!change.isChecked()) {
                Camera.Parameters parameters = camera.getParameters();
                switch (checkedId) {
                    case R.id.auto:
                        //自动闪光灯
                        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                        camera.setParameters(parameters);
                        break;
                    case R.id.open:
                        //开启闪光灯
                        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                        camera.setParameters(parameters);
                        break;
                    case R.id.close:
                        //关闭摄像头
                        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        camera.setParameters(parameters);
                        break;
                    default:
                        break;

                }
            } else {
                Toast.makeText(this, "当前摄像头没有闪光灯！", Toast.LENGTH_SHORT);
            }
        });

    }

    //响应点击事件
    CompoundButton.OnCheckedChangeListener listener = (buttonView, isChecked) -> {
        releaseCamera();
        if (isChecked) {
            cameraPosition = Camera.CameraInfo.CAMERA_FACING_FRONT;
            try {
                openCamera(holder);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            cameraPosition = Camera.CameraInfo.CAMERA_FACING_BACK;
            try {
                openCamera(holder);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    };

    private void releaseCamera() {
        //停掉原来摄像头的预览
        camera.stopPreview();
        //移除回调
        camera.setPreviewCallback(null);
        //释放资源
        camera.release();
        //取消原来摄像头
        camera = null;
    }

    /*surfaceHolder他是系统提供的一个用来设置surfaceView的一个对象，而它通过surfaceView.getHolder()这个方法来获得。
     Camera提供一个setPreviewDisplay(SurfaceHolder)的方法来连接*/
    //SurfaceHolder.Callback,这是个holder用来显示surfaceView 数据的接口,他必须实现以下3个方法
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        //当surfaceview创建时开启相机
        if (camera == null) {
            try {
                openCamera(holder);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void openCamera(SurfaceHolder holder) throws IOException {
        camera = Camera.open(cameraPosition);
        //通过surfaceview显示取景画面
        camera.setPreviewDisplay(holder);
        //获取预览相机的方向
        int sensorOrientation = getPreviewDegree(this);
        //照片输出大小
        Camera.Parameters param = camera.getParameters();
        param.setPictureSize(1920, 1080);
        //添加自动对焦
        param.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        //添加自动闪光灯
        param.setFocusMode(Camera.Parameters.FLASH_MODE_AUTO);
        camera.setParameters(param);
        camera.setDisplayOrientation(sensorOrientation);
        //添加预览回调
        camera.setPreviewCallback(this);
        //开始预览
        camera.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        //当surfaceview关闭时，关闭预览并释放资源
        releaseCamera();
        holder = null;
        surfaceView = null;
    }

    //创建jpeg图片回调数据对象
    Camera.PictureCallback jpeg = (data, camera) -> {
        // TODO Auto-generated method stub
        try {
            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            //自定义文件保存路径  以拍摄时间区分命名
            filepath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg";
            File file = new File(filepath);
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            //将图片压缩的流里面
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            // 刷新此缓冲区的输出流
            bos.flush();
            // 关闭此输出流并释放与此流有关的所有系统资源
            bos.close();
            //关闭预览 处理数据
            camera.stopPreview();
            //数据处理完后继续开始预览
            camera.startPreview();
            //回收bitmap空间
            bitmap.recycle();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    };

    private void takePhoto(Camera camera) {
        camera.takePicture(null,null,jpeg);
        //快门
        //自动对焦
     /*   camera.autoFocus((success, camera1) -> {
            // TODO Auto-generated method stub
            if (success) {
                //设置参数，并拍照
                Camera.Parameters params = camera1.getParameters();
                //图片输出格式
                params.setPictureFormat(PixelFormat.JPEG);
                //图片输出大小
                params.setPreviewSize(800, 480);
                //将参数设置到我的camera
                camera1.setParameters(params);
                //将拍摄到的照片给自定义的对象
                camera1.takePicture(null, null, jpeg);
            }
        });*/
    }


    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        Camera.Size size = camera.getParameters().getPreviewSize();
        try {
            YuvImage image = new YuvImage(data, ImageFormat.NV21, size.width, size.height, null);
            if (image != null) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image.compressToJpeg(new Rect(0, 0, size.width, size.height), 80, stream);
                Bitmap bmp = BitmapFactory.decodeByteArray(stream.toByteArray(), 0, stream.size());
                stream.close();
            }
        } catch (Exception ex) {
            Log.e("Sys", "Error:" + ex.getMessage());
        }

    }

    @OnClick(R.id.takephoto)
    public void onViewClicked() {
        takePhoto(camera);
    }
}

