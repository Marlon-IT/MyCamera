package com.example.marlon.mycamera.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.marlon.mycamera.R;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.Permission;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button openCamera1 = findViewById(R.id.open_camera_1);
        Button openCamera2 = findViewById(R.id.open_camera_2);
        openCamera1.setOnClickListener(this);
        openCamera2.setOnClickListener(this);
        requestPermission();

    }

    private void requestPermission() {
        AndPermission.with(this)
                .runtime()
                .permission(Permission.Group.CAMERA,Permission.Group.STORAGE)
                .onGranted(permissions -> {
                    //权限请求成功
                    Toast.makeText(this, "权限申请成功", Toast.LENGTH_SHORT).show();
                })
                .onDenied(permissions -> {
                    //权限请求失败
                    Toast.makeText(this, "权限申请失败", Toast.LENGTH_SHORT).show();
                })
                .start();
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.open_camera_1:
                if (AndPermission.hasPermissions(this,Permission.CAMERA)){
                    intent = new Intent(this, Camera1Activity.class);
                    startActivity(intent);
                }else {
                    requestPermission();
                }

                break;
            case R.id.open_camera_2:
                intent = new Intent(this, Camera2Activity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}
